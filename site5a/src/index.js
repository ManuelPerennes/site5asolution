import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import './css/index.css'
import './css/header.css'
import './css/footer.css'
import './css/flashInfo.css'
import './css/contact.css'

ReactDOM.render(<App />, document.getElementById('root'));
