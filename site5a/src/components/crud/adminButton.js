import DeleteInfoList from './deleteInfoList'
import UpdateInfoList from './updateInfoList'

const adminConnected = true;

function AdminButton(args) {
    const val = args.info
    if (adminConnected){
      return <span>
      <button className="CardButtonsUpdate"  onClick={()=>{console.log(val.id);UpdateInfoList(val)}}>Update</button>
      <button className="cardButtonsDelete" onClick={()=>{console.log(val.id);DeleteInfoList(val.id)}}>Delete</button>
      </span>
    }
    return null 
  }
  export default AdminButton;