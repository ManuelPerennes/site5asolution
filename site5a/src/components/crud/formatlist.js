import AdminButton from './adminButton'


function FormatList(props){
    const InfoList = props.list
    if (!InfoList ){
      return <p>Error while connecting to database, please contact the support</p>
    }    else if (InfoList.length < 0){ //@manu to be checked, le message n'apparaissait pas donc c'est sans doute ça le bon truc :)
      return <p>No News to be displayed</p>
    }      else{
      return InfoList.map((val)=>{
        return  <div className = "Formalist">
        <div className= "cardContainer" id={`container${val.id}`}>
        <div className="dynamiqueFlashinfo"><p > {val.id} Date - {val.infoDate} - <a href={val.url}>{val.texte}</a><AdminButton info={val} /></p></div>
        <div className='input' id='input'{...val.id}></div> 
        </div>
        </div>
      })
    }
  }
  export default FormatList;