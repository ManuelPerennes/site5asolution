import React, {useEffect, useState}from 'react';
import Axios from 'axios'
import FormatList from './formatlist'

const urlAPi = 'http://localhost:3001/api';


function CreateRead () { 
    const [date, setDate] = useState('');
    const [lien, setLien] = useState('');
    const [texte, setText] = useState('');
    const [InfoList, setInformationList] = useState([])
    
    useEffect(() => {
      Axios.get(urlAPi + "/getInformation").then((response) => { setInformationList(response.data) }  )
    }, [])
    
    const submitForm = () => {
      Axios.post(urlAPi + "/createInfo", 
      {
        infoDate: date,
        url: lien,
        texte: texte,
      }).then(()=> {
        //ici on set Infolist avec le dernier ajout de form
        // @manu, ici il faudrait voir comment récupérer l'ID... pour plus tard <3
        setInformationList([...InfoList, {infoDate: date, url: lien, texte: texte }])   
      })
    }; 
    return <div  className='formFlashinfo'> 
    <div className='formInput'>
    <label className="labelDate">date</label><input  className  ='inputDate' type = 'text' name='date'   onChange={(e) => {setDate(e.target.value)}}/>
    <label className="labelLien">Lien</label><input  className  ='inputLien'type = 'text' name='lien'   onChange={(e) => {setLien(e.target.value)}}/>
    <label className="labelTexte">Texte</label><input className ='inputTexte' type = 'text' name='texte'  onChange={(e) => {setText(e.target.value)}}/>
    <button onClick={submitForm}>Submit</button> 
    </div>
    <div>
     
    </div>
    <div className="resultatInfoList">
    <FormatList list={InfoList}/>
    </div>
    </div>
  }
  export default CreateRead;