import Axios from 'axios'
const urlAPi = 'http://localhost:3001/api';

const DeleteInfoList = (thisid) =>{
    return Axios.delete(`${urlAPi}/deleteList/${thisid}`)
    //ici on supprime un élément de la liste
}
export default DeleteInfoList;