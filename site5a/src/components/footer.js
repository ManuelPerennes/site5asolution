import React from 'react';

import imgFacebook from '../images/facebook.png'
import imgLinkedIn from '../images/linkedin.png'

import { Link } from 'react-router-dom';
class Footer extends React.Component {
  render() {
    return (
      <div id="footer">
        <footer>
          <div className="row">
            <div><Link to='/SiteMap'>plan du site</Link></div>
            <div>MENTIONS LEGALES<br />&copy; 5a Solutions 2021</div>
            <div>Suivez nous<br />
              <a href="https://www.facebook.com/pages/5A-Solutions/227449480770337" target="_blank" rel="noreferrer" title="Facebook">
                <img src={imgFacebook} alt="Facebook" /></a>
              <a href="http://www.linkedin.com/company/5a-solutions" target="_blank" rel="noreferrer" title="LinkedIn">
                <img src={imgLinkedIn} alt="LinkedIn" /></a>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}
export default Footer;