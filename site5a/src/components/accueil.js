import React from 'react';
import Imgaccueil from '../images/body.jpg';

class Accueil extends React.Component {
  render() {
    return <div className="blockAccueil">
      <h1 className='accueilTitre'>Accueil </h1>
     
      <div className='imgAccueilblock'>
        <img className='imgaccueil'src={Imgaccueil} alt='AccueilImag'></img>
      </div>
    </div>
  }
}
export default Accueil;