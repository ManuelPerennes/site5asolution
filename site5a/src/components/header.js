import React from 'react';
import ImgHeader from '../images/header5.png'
import ImgHeaderTxt from '../images/eadertext.png'



class Header extends React.Component {
  render() {
    return <div className='header'>
      <header className="App-header">
        <div className="imgGauche">
          <img className='imgHeader' src={ImgHeader} alt='imageHeader' />
        </div>
        <div className="imgDroite">
          <img className='imgHeadertxt' src={ImgHeaderTxt} alt='imageHeadertxt' />
        </div>
      </header>
    </div>
  }
}
export default Header;