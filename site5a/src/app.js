// App.js

import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Accueil from './components/accueil';
import Presentation from './components/presentation';
import Activites from './components/activites';
import Partenaires from './components/partenaires';
import Flashinfo from './components/flashInfo';
import Contact from './components/contact';
import SiteMap from './components/siteMap';
import Footer from './components/footer';
import Header from './components/header';

class App extends Component {
  render() {
    return (
      <Router>
        <Header />
        <div>
          <nav className="navbarblock " >

            <ul className="navbar-nav ">
              <li><Link to={'/accueil'} className="nav-linkAccueil"> Accueil </Link></li>
              <li><Link to={'/presentation'} className="nav-linkPresentation">Présentation</Link></li>
              <li><Link to={'/activites'} className="nav-linkActivite">activité</Link></li>
              <li><Link to={'/partenaires'} className="nav-linkpartenaire"> Partenaire </Link></li>
              <li><Link to={'/flashinfo'} className="nav-linkflashInfo">Flash info</Link></li>
              <li><Link to={'/contact'} className="nav-linkContact">Contact</Link></li>
            </ul>
          </nav>

          <Switch>
            <Route exact path='/' component={Accueil} />
            <Route exact path='/Accueil' component={Accueil} />
            <Route path='/presentation' component={Presentation} />
            <Route path='/activites' component={Activites} />
            <Route path='/partenaires' component={Partenaires} />
            <Route path='/flashinfo' component={Flashinfo} />
            <Route path='/siteMap' component={SiteMap} />
            <Route path='/contact' component={Contact} />
          </Switch>
        </div>
        <Footer />
      </Router>
    );
  }
}

export default App;
