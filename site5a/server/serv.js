const express = require ('express')
const mysql = require ('mysql')
const app = express()

const db = mysql.createPool({
    host:'localhost',
    user:'mysql',
    password:'mysql', 
    database: '5aSolution',
})

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"),
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"),
    res.header("Access-Control-Allow-Methods", "*")
    next()
}),

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.post("/api/createUser",(req,res) =>{   
    const nom = req.body.nom
    const prenom = req.body.prenom
    
    const sqlinsert = "INSERT INTO test (nom, prenom) VALUES (?,?)";
    db.query(sqlinsert, [nom, prenom], (err, result) => {
        console.log(err);
        res.send('inser ok') 
    });
    return 'insert into test'
});

app.post("/api/createInfo",(req,res) =>{
    const infoDate = req.body.infoDate
    const url = req.body.url
    const texte = req.body.texte
    const sqlinsert = "INSERT INTO information (infoDate, url, texte) VALUES (?,?,?)";
    db.query(sqlinsert, [infoDate, url, texte], (err, result) => { 
        console.log(err);
        res.send('inser form ok') 
    });
    return 'insert into formulaire'
});


//recupération dans la DB de la liste des infos
app.get("/api/getInformation", (req,res)=>{
    const sqlSelect = "SELECT * from information";
    db.query(sqlSelect, (err, result)=>{ res.send(result) })
})

//ici on supprime un element de la liste
app.delete("/api/deleteList/:id", (req,res)=>{
    const monId = req.params.id
    console.log(monId)
    const sqlDelete = "DELETE FROM information WHERE ? = id"
    db.query(sqlDelete, [monId], (err, result)=>{ //@manu to check, mais normalement on peut retirer monId de cette ligne
        if (err) console.log(err)
    })
})

app.listen(3001, () => {console.log ('running on port 3001')});










